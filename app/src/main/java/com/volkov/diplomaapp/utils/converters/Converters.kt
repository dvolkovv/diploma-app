package com.volkov.diplomaapp.utils.converters

import com.volkov.diplomaapp.database.entity.*
import com.volkov.diplomaapp.service.api.responses.*

fun NewsItemResponse.toEntity() =
    NewsEntity(title = this.title, date = this.date, url = this.url, info = null)

fun RuleResponse.toEntity() =
    RuleEntity(title = this.title, content = this.content, images = this.images)

fun NewsDetailInfoResponse.toEntity() =
    NewsDetailInfoEntity(content = this.content, videoUrl = this.videoUrl, photoUrl = this.photoUrl)

fun RegionResponse.toEntity() =
    RegionEntity(
        title = this.title,
        area = this.area,
        info = this.info,
        briefProgram = this.briefProgram,
        photos = this.photos,
        medAssistanceSite = this.medAssistanceSite,
        regionDetailSite = this.regionDetailSite
    )

fun DepartmentResponse.toEntity(regionId: String) = DepartmentEntity(
    regionId = regionId,
    subRegion = this.subRegion,
    oid = this.oid,
    address = this.address,
    phoneNumber = this.phoneNumber
)