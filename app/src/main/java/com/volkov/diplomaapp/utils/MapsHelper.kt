package com.volkov.diplomaapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.webkit.JavascriptInterface
import android.webkit.WebView
import com.volkov.diplomaapp.R

class MapsHelper(private val context: Context) {

    var listener: ((regionTitle: String) -> Unit)? = null
        set(value) {
            jsInterface.function = value
            field = value
        }
    private var webView: WebView? = null
    private val jsInterface = JsInterface(listener)

    @SuppressLint("SetJavaScriptEnabled")
    fun initMaps(webView: WebView) {
        this.webView = webView
        webView.settings.apply {
            javaScriptEnabled = true
            useWideViewPort = true
            loadWithOverviewMode = false
        }
        webView.addJavascriptInterface(jsInterface, "JsInterface")

        val mapReader = context.resources.openRawResource(R.raw.map).bufferedReader()
        val mapString = mapReader.readText()
        mapReader.close()

        webView.loadDataWithBaseURL(
            "http://ru.yandex.api.volkov.diplomaapp",
            mapString,
            "text/html",
            "UTF-8",
            null
        )
    }

    /**
     * Invoke this interface in js code by using following function:
     * JsInterface.invokeFunction(regionTitle)
     */
    private class JsInterface(var function: ((regionTitle: String) -> Unit)?) {
        @JavascriptInterface
        fun invokeFunction(regionTitle: String) {
            function?.invoke(regionTitle)
            Log.d("regions", regionTitle)
        }
    }
}