package com.volkov.diplomaapp.utils

import android.app.AlertDialog
import android.content.Context
import android.widget.Toast
import com.volkov.diplomaapp.R

class NotificationUtils {

    fun showToast(context: Context, resId: Int) = Toast.makeText(context, resId, Toast.LENGTH_LONG).show()

    fun showAlertDialog(context: Context, messageId: Int) {
        AlertDialog.Builder(context)
            .setMessage(messageId)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .show()
    }

}