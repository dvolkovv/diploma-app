package com.volkov.diplomaapp.utils

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import com.volkov.diplomaapp.R

fun Context.getPhotoIdByString(imageTitle: String): Int {
    return this.resources.getIdentifier(imageTitle, "drawable", this.packageName)
}

fun getProgressDrawable(context: Context): CircularProgressDrawable {
    return CircularProgressDrawable(context).apply {
        val color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
        strokeWidth = 5f
        centerRadius = 30f
        setColorSchemeColors(color)
        start()
    }
}

