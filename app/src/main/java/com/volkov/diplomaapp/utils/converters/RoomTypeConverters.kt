package com.volkov.diplomaapp.utils.converters

import android.arch.persistence.room.TypeConverter
import android.net.Uri
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*

class RoomTypeConverters {

    @TypeConverter
    fun urisToString(value: List<Uri>?): String? {
        if (value == null) {
            return null
        }
        if (value.isEmpty()) {
            return ""
        }
        return value.joinToString()
    }

    @TypeConverter
    fun urisStringToList(urisString: String?): List<Uri>? {
        if (urisString == null) {
            return emptyList()
        }
        if (urisString.isEmpty()) {
            return emptyList()
        }
        return urisString.split(", ").map(Uri::parse)
    }

    @TypeConverter
    fun dateToInt(date: Date) = date.time

    @TypeConverter
    fun intToDate(dateInt: Long) = Date(dateInt)

    private val itemType: Type = object : TypeToken<List<String>>() {}.type

    @TypeConverter
    fun listToJson(list: List<String>?): String = Gson().toJson(list, itemType)

    @TypeConverter
    fun jsonToList(listString: String): List<String> {
        val list: List<String>? = Gson().fromJson(listString, itemType)
        return list.orEmpty()
    }

}