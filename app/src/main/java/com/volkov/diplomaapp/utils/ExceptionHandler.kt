package com.volkov.diplomaapp.utils

import android.content.Context
import com.crashlytics.android.Crashlytics
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.R
import io.reactivex.functions.Consumer
import java.net.UnknownHostException
import javax.inject.Inject

class ExceptionHandler(val context: Context?) : Consumer<Throwable> {
    @Inject
    lateinit var notificationBuilder: NotificationUtils

    init {
        DiplomaApp.appComponent.inject(this)
    }

    override fun accept(t: Throwable) {
        t.printStackTrace()
        when (t) {
            is UnknownHostException -> showAlertDialog(R.string.connection_problem)
            else -> {
                Crashlytics.logException(t)
                showToast(R.string.unknown_problem)
            }
        }
    }

    private fun showToast(resId: Int) = context?.let { notificationBuilder.showToast(it, resId) }

    private fun showAlertDialog(resId: Int) =
        context?.let { notificationBuilder.showAlertDialog(it, resId) }
}