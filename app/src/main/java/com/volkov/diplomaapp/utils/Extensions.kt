package com.volkov.diplomaapp.utils

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.animation.Animation
import java.util.*

fun View.toggleVisibility(isVisible: Boolean) {
    if (isVisible) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}

fun View.flipVisibility() {
    when (this.visibility) {
        View.VISIBLE -> this.visibility = View.GONE
        View.GONE, View.INVISIBLE -> this.visibility = View.VISIBLE
    }
}

fun String.fromHtml(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this)
    }
}

fun Animation.setAnimationCallback(
    onStart: ((Animation?) -> Unit)? = null,
    onRepeat: ((Animation?) -> Unit)? = null,
    onEnd: ((Animation?) -> Unit)? = null
) {
    this.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationRepeat(animation: Animation?) {
            onRepeat?.invoke(animation)
        }

        override fun onAnimationEnd(animation: Animation?) {
            onEnd?.invoke(animation)
        }

        override fun onAnimationStart(animation: Animation?) {
            onStart?.invoke(animation)
        }
    })
}

fun Calendar.setTime(hours: Int?, minutes: Int?) {
    if (hours != null) {
        this.set(Calendar.HOUR_OF_DAY, hours)
    }
    if (minutes != null) {
        this.set(Calendar.MINUTE, minutes)
    }
}
