package com.volkov.diplomaapp.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.volkov.diplomaapp.database.entity.RuleEntity
import io.reactivex.Single

@Dao
interface RulesDao : BaseDao<RuleEntity> {

    @Query("Select * from RuleEntity")
    fun getRules(): Single<List<RuleEntity>>

}
