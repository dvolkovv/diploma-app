package com.volkov.diplomaapp.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.volkov.diplomaapp.database.entity.NewsEntity
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface NewsDao : BaseDao<NewsEntity> {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addWithIgnore(list: List<NewsEntity>)

    @Query("Delete from NewsEntity")
    fun removeAll()

    @Query("Select * from NewsEntity order by date desc")
    fun getAllNews(): Flowable<List<NewsEntity>>

    @Query("Select * from NewsEntity where id = :newsId")
    fun getSingleNews(newsId: Int): Single<NewsEntity>

}
