package com.volkov.diplomaapp.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.volkov.diplomaapp.database.access.RegionWithDepartments
import com.volkov.diplomaapp.database.entity.RegionEntity
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface RegionDao : BaseDao<RegionEntity> {

    @Query("Select * from RegionEntity")
    fun getAllRegions(): Single<List<RegionEntity>>

    @Query("Select * from RegionEntity where title = :regionId")
    @Transaction
    fun getRegionWithDepartments(regionId: String): Flowable<RegionWithDepartments>

    @Query("Select * from RegionEntity where title = :regionId")
    fun getRegion(regionId: String): Single<RegionEntity>

}
