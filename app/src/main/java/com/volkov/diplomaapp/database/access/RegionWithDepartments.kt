package com.volkov.diplomaapp.database.access

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import com.volkov.diplomaapp.database.entity.DepartmentEntity
import com.volkov.diplomaapp.database.entity.RegionEntity

class RegionWithDepartments {
    @Embedded
    lateinit var regionEntity: RegionEntity
    @Relation(parentColumn = "title", entityColumn = "regionId")
    lateinit var departments: List<DepartmentEntity>
}
