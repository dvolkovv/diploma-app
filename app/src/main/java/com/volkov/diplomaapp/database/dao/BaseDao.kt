package com.volkov.diplomaapp.database.dao

import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Update

interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(entity: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(list: List<T>)

    @Delete
    fun remove(vararg entity: T)

    @Update
    fun update(vararg entity: T)
}