package com.volkov.diplomaapp.database.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = RegionEntity::class,
        parentColumns = ["title"],
        childColumns = ["regionId"],
        onDelete = CASCADE,
        onUpdate = CASCADE
    )], indices = [Index("regionId")]
)
data class DepartmentEntity(
    val regionId: String,
    val subRegion: String,
    val oid: String,
    val address: String,
    val phoneNumber: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}