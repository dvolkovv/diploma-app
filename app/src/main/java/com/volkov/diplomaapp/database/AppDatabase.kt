package com.volkov.diplomaapp.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.volkov.diplomaapp.BuildConfig
import com.volkov.diplomaapp.database.dao.DepartmentDao
import com.volkov.diplomaapp.database.dao.NewsDao
import com.volkov.diplomaapp.database.dao.RegionDao
import com.volkov.diplomaapp.database.dao.RulesDao
import com.volkov.diplomaapp.database.entity.DepartmentEntity
import com.volkov.diplomaapp.database.entity.NewsEntity
import com.volkov.diplomaapp.database.entity.RegionEntity
import com.volkov.diplomaapp.database.entity.RuleEntity
import com.volkov.diplomaapp.utils.converters.RoomTypeConverters

@Database(
    entities = [RegionEntity::class, NewsEntity::class, RuleEntity::class, DepartmentEntity::class],
    version = BuildConfig.VERSION_CODE,
    exportSchema = false
)
@TypeConverters(RoomTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun regionDao(): RegionDao
    abstract fun newsDao(): NewsDao
    abstract fun rulesDao(): RulesDao
    abstract fun departmentDao(): DepartmentDao
}