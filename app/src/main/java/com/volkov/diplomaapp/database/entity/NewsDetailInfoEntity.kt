package com.volkov.diplomaapp.database.entity

import android.net.Uri

/**
 * content - html text
 */
data class NewsDetailInfoEntity(
    val content: String,
    val videoUrl: List<String>?,
    val photoUrl: List<Uri>?
)