package com.volkov.diplomaapp.database.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.net.Uri
import com.volkov.diplomaapp.utils.setTime
import java.text.SimpleDateFormat
import java.util.*

@Entity
data class NewsEntity(
    val title: String,
    val date: Date,
    val url: String,
    @Embedded
    var info: NewsDetailInfoEntity?
) {
    @PrimaryKey
    var id: Int = Uri.parse(url).lastPathSegment?.toInt()!!

    constructor(
        title: String,
        date: String,
        url: String,
        info: NewsDetailInfoEntity?
    ) : this(title, parseDate(date), url, info)

    fun getDateString(): String = dateFormatter.format(date)

    companion object {
        private val dateFormatter = SimpleDateFormat("dd MMM HH:mm", Locale("ru"))

        private fun parseDate(dateString: String): Date {
            return if (dateString.contains("сегодня", true)) {
                val resultDate = Calendar.getInstance()
                val regex = "([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]".toRegex()
                val timeString = regex.find(dateString)?.value
                SimpleDateFormat("HH:mm", Locale("ru")).parse(timeString).let {
                    resultDate.setTime(it.hours, it.minutes)
                }
                resultDate.time
            } else {
                dateFormatter.parse(dateString)
            }
        }
    }
}