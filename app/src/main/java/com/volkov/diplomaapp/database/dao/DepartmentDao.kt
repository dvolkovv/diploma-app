package com.volkov.diplomaapp.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.volkov.diplomaapp.database.entity.DepartmentEntity
import io.reactivex.Single

@Dao
interface DepartmentDao : BaseDao<DepartmentEntity> {

    @Query("Select * from DepartmentEntity where regionId = :regionId")
    fun getDepartmensByRegion(regionId: String): Single<List<DepartmentEntity>>
}