package com.volkov.diplomaapp.database.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class RegionEntity(
    @SerializedName("region") @PrimaryKey val title: String,
    val area: Int,
    val info: String?,
    val photos: List<String>?,
    val briefProgram: String?,
    var medAssistanceSite: String?,
    var regionDetailSite: String?
) {
    fun isDescriptionInfoEmpty(): Boolean {
        return photos.isNullOrEmpty() && info.isNullOrBlank()
    }

    fun isRegionProgramsEmpty() = briefProgram == null && regionDetailSite == null

}
