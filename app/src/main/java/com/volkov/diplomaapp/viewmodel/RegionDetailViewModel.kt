package com.volkov.diplomaapp.viewmodel

import android.arch.lifecycle.ViewModel
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.database.access.RegionWithDepartments
import com.volkov.diplomaapp.database.entity.DepartmentEntity
import com.volkov.diplomaapp.database.entity.RegionEntity
import com.volkov.diplomaapp.service.model.RegionModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

/**
 * To get shared instance of this viewmodel use static method Fragment.getSharedRegionViewModel()
 */
class RegionDetailViewModel : ViewModel() {
    @Inject
    lateinit var model: RegionModel
    var selectedRegionId: String? = null
        set(value) {
            field = value
            loadRegionWithDepartments()
        }

    private val currentRegion =
        BehaviorSubject.create<RegionWithDepartments>()
    private val loadingSubject = BehaviorSubject.create<Boolean>()
    private var subscription: Disposable? = null

    init {
        DiplomaApp.appComponent.inject(this)
    }

    fun getCurrentRegionFull(): Observable<RegionWithDepartments> =
        currentRegion.observeOn(AndroidSchedulers.mainThread())

    fun getCurrentRegionDepartments(): Observable<List<DepartmentEntity>> =
        currentRegion.map { it.departments }.observeOn(AndroidSchedulers.mainThread())

    fun getCurrentRegionEntity(): Observable<RegionEntity> =
        currentRegion.map { it.regionEntity }.observeOn(AndroidSchedulers.mainThread())

    fun getCurrentRegionPhotos(): Observable<List<String>> =
        currentRegion.map {
            it.regionEntity.photos ?: emptyList()
        }.observeOn(AndroidSchedulers.mainThread())

    private fun loadRegionWithDepartments() {
        subscription = model.getRegionWithDepartments(selectedRegionId)
            .doOnSubscribe { loadingSubject.onNext(true) }
            .doOnTerminate { loadingSubject.onNext(false) }
            .subscribe({ currentRegion.onNext(it) },
                { currentRegion.onError(it) },
                { currentRegion.onComplete() })
    }

    override fun onCleared() {
        super.onCleared()
        subscription?.dispose()
    }

}