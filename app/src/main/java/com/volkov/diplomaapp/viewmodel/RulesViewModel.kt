package com.volkov.diplomaapp.viewmodel

import android.arch.lifecycle.ViewModel
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.database.entity.RuleEntity
import com.volkov.diplomaapp.service.model.RulesModel
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class RulesViewModel : ViewModel() {

    @Inject
    lateinit var model: RulesModel
    var rulesObservable: Flowable<List<RuleEntity>>
    private val loadingSubject = BehaviorSubject.create<Boolean>()
    private val exception = PublishSubject.create<Throwable>()

    init {
        DiplomaApp.appComponent.inject(this)
        rulesObservable = createRulesObservable()
    }

    fun getLoadingObservable(): Observable<Boolean> =
        loadingSubject.observeOn(AndroidSchedulers.mainThread())

    fun getExceptionObservable(): Observable<Throwable> =
        exception.observeOn(AndroidSchedulers.mainThread())

    private fun createRulesObservable(): Flowable<List<RuleEntity>> {
        return model.getRules()
            .doOnSubscribe { loadingSubject.onNext(true) }
            .doOnEach { loadingSubject.onNext(false) }
            .doOnError { exception.onNext(it) }
            .distinct()
            .observeOn(AndroidSchedulers.mainThread())
    }


}