package com.volkov.diplomaapp.viewmodel

import android.arch.lifecycle.ViewModel
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.service.model.EntranceModel
import javax.inject.Inject

class SplashViewModel : ViewModel() {

    @Inject
    lateinit var entranceModel: EntranceModel

    init {
        DiplomaApp.appComponent.inject(this)
    }

    fun isAppFirstLaunch() = entranceModel.isFirstLaunch

}
