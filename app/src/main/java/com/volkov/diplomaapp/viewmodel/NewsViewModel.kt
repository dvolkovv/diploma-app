package com.volkov.diplomaapp.viewmodel

import android.arch.lifecycle.ViewModel
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.database.entity.NewsEntity
import com.volkov.diplomaapp.service.model.NewsModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class NewsViewModel : ViewModel() {

    var newsObservable: Flowable<List<NewsEntity>>
    @Inject
    lateinit var model: NewsModel

    private val exceptionSubject = BehaviorSubject.create<Throwable>()
    private val loadingSubject = BehaviorSubject.create<Boolean>()

    init {
        DiplomaApp.appComponent.inject(this)
        newsObservable = createNewsObservable()
    }

    fun getLoadingObservable(): Observable<Boolean> =
        loadingSubject.observeOn(AndroidSchedulers.mainThread())

    fun getExceptionObservable(): Observable<Throwable> =
        exceptionSubject.observeOn(AndroidSchedulers.mainThread())

    fun refreshNews(): Completable =
        model.refreshNews().observeOn(AndroidSchedulers.mainThread())
            .onErrorComplete { t: Throwable ->
                exceptionSubject.onNext(t)
                true
            }

    private fun createNewsObservable() =
        model.getNews()
            .doOnSubscribe { loadingSubject.onNext(true) }
            .doOnEach { loadingSubject.onNext(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { t: Throwable ->
                exceptionSubject.onNext(t)
                Flowable.empty()
            }

}