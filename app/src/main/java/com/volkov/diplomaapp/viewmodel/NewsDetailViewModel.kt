package com.volkov.diplomaapp.viewmodel

import android.arch.lifecycle.ViewModel
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.database.entity.NewsEntity
import com.volkov.diplomaapp.service.model.NewsModel
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class NewsDetailViewModel : ViewModel() {
    var newsObservable: Single<NewsEntity>? = null
    @Inject
    lateinit var model: NewsModel

    private val loadingSubject = BehaviorSubject.create<Boolean>()

    init {
        DiplomaApp.appComponent.inject(this)
    }

    fun getLoadingObservable(): Observable<Boolean> =
        loadingSubject.observeOn(AndroidSchedulers.mainThread())

    fun createNewsObservable(newsId: Int) {
        newsObservable = model.getSingleNews(newsId)
            .doOnSubscribe { loadingSubject.onNext(true) }
            .doOnEvent { _, _ -> loadingSubject.onNext(false) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}