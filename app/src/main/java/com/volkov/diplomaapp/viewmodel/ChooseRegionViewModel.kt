package com.volkov.diplomaapp.viewmodel

import android.arch.lifecycle.ViewModel
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.database.entity.RegionEntity
import com.volkov.diplomaapp.service.model.RegionModel
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ChooseRegionViewModel : ViewModel() {
    @Inject
    lateinit var regionModel: RegionModel

    val regions: Single<List<RegionEntity>>

    private val queryText: BehaviorSubject<String> = BehaviorSubject.create()

    init {
        DiplomaApp.appComponent.inject(this)
        regions = createRegionsObservable()
    }

    fun updateQueryText(query: String) {
        queryText.onNext(query)
    }

    fun getQueryText(): Observable<String> {
        return queryText.debounce(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun createRegionsObservable() =
        regionModel.getRegions()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

}