package com.volkov.diplomaapp.viewmodel

import android.arch.lifecycle.ViewModel
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.service.model.EntranceModel
import javax.inject.Inject

class EntranceViewModel : ViewModel() {

    @Inject
    lateinit var prefsModel: EntranceModel

    init {
        DiplomaApp.appComponent.inject(this)
    }

    fun setAppFirstLaunch(isFirstLaunch: Boolean) {
        prefsModel.isFirstLaunch = isFirstLaunch
    }

}
