package com.volkov.diplomaapp

import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import androidx.work.*
import com.volkov.diplomaapp.di.*
import com.volkov.diplomaapp.service.workmanager.UpdateWorkManager
import java.util.concurrent.TimeUnit


class DiplomaApp : MultiDexApplication() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initPeriodicNewsUpdates()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        appComponent = initAppComponent()
    }

    private fun initPeriodicNewsUpdates() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val workRequest =
            PeriodicWorkRequest.Builder(UpdateWorkManager::class.java, 12, TimeUnit.HOURS)
                .setConstraints(constraints)
                .build()
        WorkManager.getInstance()
            .enqueueUniquePeriodicWork(
                UpdateWorkManager.UPDATE_WORK_ID,
                ExistingPeriodicWorkPolicy.KEEP,
                workRequest
            )
    }

    private fun initAppComponent(): AppComponent {
        return DaggerAppComponent.builder()
            .appModule(AppModule(applicationContext))
            .databaseModule(DatabaseModule(applicationContext))
            .modelsModule(ModelsModule())
            .apiModule(ApiModule())
            .build()
    }
}