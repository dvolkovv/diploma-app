package com.volkov.diplomaapp.di

import android.content.Context
import com.volkov.diplomaapp.utils.MapsHelper
import com.volkov.diplomaapp.utils.NotificationUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val appContext: Context) {
    @Provides
    @Singleton
    fun provideContext(): Context {
        return appContext
    }

    @Provides
    @Singleton
    fun provideMapHelper(): MapsHelper {
        return MapsHelper(appContext)
    }

    @Provides
    @Singleton
    fun provideDialogUtils(): NotificationUtils {
        return NotificationUtils()
    }

}