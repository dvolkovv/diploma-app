package com.volkov.diplomaapp.di

import android.content.Context
import android.support.annotation.NonNull
import com.volkov.diplomaapp.service.model.EntranceModel
import com.volkov.diplomaapp.service.model.NewsModel
import com.volkov.diplomaapp.service.model.RegionModel
import com.volkov.diplomaapp.service.model.RulesModel
import com.volkov.diplomaapp.service.repository.DepartmentsRepository
import com.volkov.diplomaapp.service.repository.NewsRepository
import com.volkov.diplomaapp.service.repository.RegionsRepository
import com.volkov.diplomaapp.service.repository.RulesRepository
import dagger.Module
import dagger.Provides

@Module
class ModelsModule {

    @Provides
    @NonNull
    fun provideEntranceModel(
        context: Context
    ) = EntranceModel(context)

    @Provides
    @NonNull
    fun provideNewsModel(newsRepository: NewsRepository) = NewsModel(newsRepository)


    @Provides
    @NonNull
    fun provideRulesModel(rulesRepository: RulesRepository) = RulesModel(rulesRepository)

    @Provides
    @NonNull
    fun provideRegionModel(
        regionsRepository: RegionsRepository,
        departmentsRepository: DepartmentsRepository
    ) = RegionModel(regionsRepository, departmentsRepository)

}