package com.volkov.diplomaapp.di

import com.volkov.diplomaapp.service.workmanager.UpdateWorkManager
import com.volkov.diplomaapp.utils.ExceptionHandler
import com.volkov.diplomaapp.view.ui.ChooseRegionFragment
import com.volkov.diplomaapp.viewmodel.*
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, DatabaseModule::class, ModelsModule::class, ApiModule::class, RepositoryModule::class])
@Singleton
interface AppComponent {
    fun inject(splashViewModel: SplashViewModel)
    fun inject(entranceViewModel: EntranceViewModel)
    fun inject(chooseRegionViewModel: ChooseRegionViewModel)
    fun inject(chooseRegionFragment: ChooseRegionFragment)
    fun inject(exceptionHandler: ExceptionHandler)
    fun inject(newsViewModel: NewsViewModel)
    fun inject(newsDetailViewModel: NewsDetailViewModel)
    fun inject(updateWorkManager: UpdateWorkManager)
    fun inject(rulesViewModel: RulesViewModel)
    fun inject(regionDetailViewModel: RegionDetailViewModel)
}