package com.volkov.diplomaapp.di

import android.content.Context
import com.volkov.diplomaapp.database.dao.NewsDao
import com.volkov.diplomaapp.database.dao.RegionDao
import com.volkov.diplomaapp.database.dao.RulesDao
import com.volkov.diplomaapp.service.api.NewsApiService
import com.volkov.diplomaapp.service.api.RulesApiService
import com.volkov.diplomaapp.service.repository.DepartmentsRepository
import com.volkov.diplomaapp.service.repository.NewsRepository
import com.volkov.diplomaapp.service.repository.RegionsRepository
import com.volkov.diplomaapp.service.repository.RulesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideRegionsRepository(
        context: Context,
        regionsDao: RegionDao,
        departmentsRepository: DepartmentsRepository
    ) =
        RegionsRepository(context, regionsDao, departmentsRepository)

    @Provides
    @Singleton
    fun provideNewsRepository(newsDao: NewsDao, newsApiService: NewsApiService) =
        NewsRepository(newsDao, newsApiService)

    @Provides
    @Singleton
    fun provideRulesRepository(rulesDao: RulesDao, rulesApiService: RulesApiService) =
        RulesRepository(rulesDao, rulesApiService)

}