package com.volkov.diplomaapp.di

import android.arch.persistence.room.Room
import android.content.Context
import com.volkov.diplomaapp.BuildConfig
import com.volkov.diplomaapp.database.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(context: Context) {
    private val database: AppDatabase = initDatabase(context)

    @Provides
    @Singleton
    fun provideRegionDao() = database.regionDao()

    @Provides
    @Singleton
    fun provideNewsDao() = database.newsDao()

    @Provides
    @Singleton
    fun provideRulesDao() = database.rulesDao()

    @Provides
    @Singleton
    fun provideDepartmentsDao() = database.departmentDao()

    @Provides
    @Singleton
    fun provideAppDatabase() = database

    private fun initDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, BuildConfig.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }
}