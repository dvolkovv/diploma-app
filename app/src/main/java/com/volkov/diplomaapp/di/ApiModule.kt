package com.volkov.diplomaapp.di

import com.volkov.diplomaapp.service.api.NewsApiService
import com.volkov.diplomaapp.service.api.RulesApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideNewsService(): NewsApiService =
        NewsApiService()

    @Provides
    @Singleton
    fun provideRulesService(): RulesApiService =
        RulesApiService()
}