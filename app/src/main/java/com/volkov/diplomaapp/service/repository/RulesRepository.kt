package com.volkov.diplomaapp.service.repository

import com.volkov.diplomaapp.database.dao.RulesDao
import com.volkov.diplomaapp.database.entity.RuleEntity
import com.volkov.diplomaapp.service.api.RulesApiService
import com.volkov.diplomaapp.service.api.responses.RuleResponse
import com.volkov.diplomaapp.utils.converters.toEntity
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RulesRepository @Inject constructor(
    private val rulesDao: RulesDao,
    private val rulesApiService: RulesApiService
) : Repository<RuleEntity> {

    override fun add(list: List<RuleEntity>) {
        rulesDao.add(list)
    }

    override fun update(vararg t: RuleEntity) {
        rulesDao.update(*t)
    }

    override fun remove(vararg t: RuleEntity) {
        rulesDao.remove(*t)
    }

    fun getAllRules(): Flowable<List<RuleEntity>> {
        return Single.merge(
            getFromDatabase(), getFromRemoteWithSave()
        )
    }

    private fun getFromDatabase() = rulesDao.getRules().subscribeOn(Schedulers.io())

    private fun getFromRemoteWithSave() =
        rulesApiService.getRulesFromRemote()
            .retryWhen { it.delay(5, TimeUnit.SECONDS) }
            .map {
                it.map(RuleResponse::toEntity)
            }
            .doOnSuccess { add(it) }
            .subscribeOn(Schedulers.io())

}
