package com.volkov.diplomaapp.service.api.responses

import com.google.gson.annotations.SerializedName

class DepartmentResponse(
    @SerializedName("subRegion") val subRegion: String,
    @SerializedName("address") val address: String,
    @SerializedName("oid") val oid: String,
    @SerializedName("phoneNumber") val phoneNumber: String
)