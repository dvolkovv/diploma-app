package com.volkov.diplomaapp.service.model

import com.volkov.diplomaapp.database.access.RegionWithDepartments
import com.volkov.diplomaapp.database.entity.DepartmentEntity
import com.volkov.diplomaapp.database.entity.RegionEntity
import com.volkov.diplomaapp.service.repository.DepartmentsRepository
import com.volkov.diplomaapp.service.repository.RegionsRepository
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RegionModel @Inject constructor(
    private val regionRepository: RegionsRepository,
    private val departmentsRepository: DepartmentsRepository
) {

    fun getRegions(): Single<List<RegionEntity>> {
        return regionRepository.getAllRegions()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getRegion(regionId: String?): Single<RegionEntity> {
        return if (regionId == null) {
            Single.error(IllegalArgumentException("Region ID must be present"))
        } else {
            regionRepository.getRegion(regionId).subscribeOn(Schedulers.io())
        }
    }

    fun getDepartmentsByRegion(regionId: String?): Single<List<DepartmentEntity>>? {
        return if (regionId == null) {
            Single.error(IllegalArgumentException("Region ID must be present"))
        } else {
            departmentsRepository.getDepartmentsByRegion(regionId)
                .subscribeOn(Schedulers.io())
        }
    }

    fun getRegionWithDepartments(regionId: String?): Flowable<RegionWithDepartments> {
        return if (regionId == null) {
            Flowable.error(IllegalArgumentException("Region ID must be present!"))
        } else {
            regionRepository.getRegionWithDepartments(regionId)
                .subscribeOn(Schedulers.io())
        }
    }


}