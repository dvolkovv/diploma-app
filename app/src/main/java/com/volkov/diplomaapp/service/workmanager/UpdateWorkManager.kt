package com.volkov.diplomaapp.service.workmanager

import android.content.Context
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.service.model.NewsModel
import io.reactivex.Single
import javax.inject.Inject

class UpdateWorkManager(context: Context, params: WorkerParameters) : RxWorker(context, params) {

    @Inject
    lateinit var model: NewsModel

    companion object {
        const val UPDATE_WORK_ID = "update_work_id"
    }

    init {
        DiplomaApp.appComponent.inject(this)
    }

    override fun createWork(): Single<Result> {
        with(model) {
            return dropNews().concatWith(refreshNews()).toSingleDefault(Result.success())
                .onErrorReturnItem(Result.failure())
        }
    }
}