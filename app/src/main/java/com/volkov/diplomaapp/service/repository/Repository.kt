package com.volkov.diplomaapp.service.repository

interface Repository<T> {
    fun add(t: T) {
        add(listOf(t))
    }

    fun add(list: List<T>)

    fun update(vararg t: T)

    fun remove(vararg t: T)
}