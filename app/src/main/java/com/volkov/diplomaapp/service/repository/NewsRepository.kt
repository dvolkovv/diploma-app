package com.volkov.diplomaapp.service.repository

import android.util.Log
import com.volkov.diplomaapp.database.dao.NewsDao
import com.volkov.diplomaapp.database.entity.NewsDetailInfoEntity
import com.volkov.diplomaapp.database.entity.NewsEntity
import com.volkov.diplomaapp.service.api.NewsApiService
import com.volkov.diplomaapp.service.api.responses.NewsDetailInfoResponse
import com.volkov.diplomaapp.service.api.responses.NewsItemResponse
import com.volkov.diplomaapp.utils.converters.toEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val NEWS_LOAD_TAG = "News Downloaded"

class NewsRepository @Inject constructor(
    private val newsDao: NewsDao,
    private val newsApiService: NewsApiService
) : Repository<NewsEntity> {

    override fun add(list: List<NewsEntity>) {
        newsDao.addWithIgnore(list)
    }

    override fun update(vararg t: NewsEntity) {
        newsDao.update(*t)
    }

    override fun remove(vararg t: NewsEntity) {
        newsDao.remove(*t)
    }

    fun removeAllNews(): Completable {
        return Completable.fromCallable {
            newsDao.removeAll()
        }
    }

    fun getAllNews(): Flowable<List<NewsEntity>> {
        return getAllNewsFromDatabase()
            .switchIfEmpty(
                getAllNewsFromRemote().toFlowable()
            )
    }

    fun getSingleNews(newsId: Int): Single<NewsEntity> {
        return newsDao.getSingleNews(newsId)
    }

    fun getNewsDetailInfoFromRemote(newsEntity: NewsEntity): Single<NewsDetailInfoEntity> {
        return newsApiService.getNewsDetailInfoFromRemote(newsEntity)
            .retryWhen { it.delay(5, TimeUnit.SECONDS) }
            .map(NewsDetailInfoResponse::toEntity)
    }

    fun refreshNews(): Completable = getAllNewsFromRemote().ignoreElement()

    private fun getAllNewsFromRemote(): Single<List<NewsEntity>> {
        return Single.fromCallable(newsApiService::getNewsFromRemote)
            .retryWhen { it.delay(5, TimeUnit.SECONDS) }
            .map {
                it.map(NewsItemResponse::toEntity)
            }
            .doOnSuccess { newsList ->
                Log.d(NEWS_LOAD_TAG, newsList.toString())
                add(newsList)
            }
    }

    private fun getAllNewsFromDatabase() = newsDao.getAllNews()
        .take(1)
        .filter(List<NewsEntity>::isNotEmpty)

}