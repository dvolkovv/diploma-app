package com.volkov.diplomaapp.service.model

import com.volkov.diplomaapp.database.entity.NewsDetailInfoEntity
import com.volkov.diplomaapp.database.entity.NewsEntity
import com.volkov.diplomaapp.service.repository.NewsRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class NewsModel @Inject constructor(
    private val newsRepository: NewsRepository
) {

    fun getSingleNews(newsId: Int): Single<NewsEntity> =
        newsRepository.getSingleNews(newsId)
            .flatMap {
                //todo refactor
                if (it.info == null) {
                    getNewsDetailInfo(it).flatMap { details ->
                        it.info = details
                        newsRepository.update(it)
                        Single.just(it)
                    }
                } else Single.just(it)
            }
            .subscribeOn(Schedulers.io())

    fun getNews(): Flowable<List<NewsEntity>> =
        newsRepository.getAllNews()
            .subscribeOn(Schedulers.io())

    fun refreshNews(): Completable =
        newsRepository.refreshNews()
            .subscribeOn(Schedulers.io())

    fun dropNews(): Completable = newsRepository.removeAllNews().subscribeOn(Schedulers.io())

    private fun getNewsDetailInfo(newsEntity: NewsEntity): Single<NewsDetailInfoEntity> {
        return newsRepository.getNewsDetailInfoFromRemote(newsEntity).subscribeOn(Schedulers.io())
    }


}