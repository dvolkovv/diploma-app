package com.volkov.diplomaapp.service.api.responses

import android.net.Uri

data class NewsDetailInfoResponse(
    val content: String,
    val videoUrl: List<String>?,
    val photoUrl: List<Uri>?
)