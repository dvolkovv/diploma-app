package com.volkov.diplomaapp.service.repository

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.access.RegionWithDepartments
import com.volkov.diplomaapp.database.dao.RegionDao
import com.volkov.diplomaapp.database.entity.RegionEntity
import com.volkov.diplomaapp.service.api.responses.RegionResponse
import com.volkov.diplomaapp.utils.converters.toEntity
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class RegionsRepository @Inject constructor(
    private val context: Context,
    private val regionDao: RegionDao,
    private val departmentsRepository: DepartmentsRepository
) : Repository<RegionEntity> {

    override fun add(list: List<RegionEntity>) {
        regionDao.add(list)
    }

    override fun update(vararg t: RegionEntity) {
        regionDao.update(*t)
    }

    override fun remove(vararg t: RegionEntity) {
        regionDao.remove(*t)
    }

    fun getAllRegions(): Single<List<RegionEntity>> {
        return regionDao.getAllRegions().filter { it.isNotEmpty() }
            .switchIfEmpty(getRegionsFromRemote())
    }

    fun getRegion(regionId: String): Single<RegionEntity> {
        return regionDao.getRegion(regionId)
    }

    fun getRegionWithDepartments(regionId: String): Flowable<RegionWithDepartments> {
        return regionDao.getRegionWithDepartments(regionId)
    }

    private fun getRegionsFromRemote(): Single<List<RegionEntity>> {
        return Single.fromCallable {
            val itemType = object : TypeToken<List<RegionResponse>>() {}.type
            val input = context.resources.openRawResource(R.raw.regions).reader()
            val list: List<RegionResponse> = Gson().fromJson(input.readText(), itemType)
            val regionEntities = list.map { it.toEntity() }
            add(regionEntities)
            list.forEach {
                val regionEntity = it.toEntity()
                if (it.departments != null) {
                    it.departments.map { dep -> dep.toEntity(regionEntity.title) }
                        .let(departmentsRepository::add)
                }
            }
            return@fromCallable regionEntities
        }
    }
}