package com.volkov.diplomaapp.service.api.responses

import android.net.Uri

data class RuleResponse(
    val title: String,
    val content: String,
    val images: List<Uri>
)