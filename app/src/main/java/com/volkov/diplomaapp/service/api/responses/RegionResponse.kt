package com.volkov.diplomaapp.service.api.responses

import com.google.gson.annotations.SerializedName

class RegionResponse(
    @SerializedName("region") val title: String,
    @SerializedName("area") val area: Int,
    @SerializedName("info") val info: String?,
    @SerializedName("photos") val photos: List<String>?,
    @SerializedName("departments") val departments: List<DepartmentResponse>?,
    @SerializedName("regionBriefProgram") val briefProgram: String?,
    @SerializedName("medAssistanceSite") val medAssistanceSite: String?,
    @SerializedName("regionDetailSite") val regionDetailSite: String?
)