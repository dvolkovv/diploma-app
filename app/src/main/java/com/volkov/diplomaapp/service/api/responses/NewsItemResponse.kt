package com.volkov.diplomaapp.service.api.responses

class NewsItemResponse(
        val title: String,
        val date: String,
        val url: String
)