package com.volkov.diplomaapp.service.model

import com.volkov.diplomaapp.database.entity.RuleEntity
import com.volkov.diplomaapp.service.repository.RulesRepository
import io.reactivex.Flowable
import javax.inject.Inject

class RulesModel @Inject constructor(
    private val rulesRepository: RulesRepository
) {
    fun getRules(): Flowable<List<RuleEntity>> = rulesRepository.getAllRules().filter(List<RuleEntity>::isNotEmpty)
}