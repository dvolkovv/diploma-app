package com.volkov.diplomaapp.service.api

import android.net.Uri
import com.volkov.diplomaapp.BuildConfig
import com.volkov.diplomaapp.service.api.responses.RuleResponse
import io.reactivex.Single
import org.jsoup.Jsoup
import org.jsoup.select.Elements
import java.util.concurrent.TimeUnit

class RulesApiService {

    private val connection = {
        Jsoup.connect(BuildConfig.BASE_RULES_URL)
    }

    fun getRulesFromRemote(): Single<List<RuleResponse>> {
        return Single.fromCallable(connection)
            .map { it.get().allElements.let(::parseRules) }
            .delaySubscription(2, TimeUnit.SECONDS)
    }

    private fun parseRules(elements: Elements): List<RuleResponse> {
        val result = ArrayList<RuleResponse>()
        val rulesElements = elements.select("blockquote")
        rulesElements.forEach {
            val ruleTitle = it.getElementsByTag("h3").text()
            val ruleImages =
                it.getElementsByTag("img").map { img -> Uri.parse(img.attr("src")) }
            it.select("img").remove()
            val ruleContent = it.html()
            result.add(RuleResponse(ruleTitle, ruleContent, ruleImages))
        }
        return result
    }
}
