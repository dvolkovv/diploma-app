package com.volkov.diplomaapp.service.repository

import com.volkov.diplomaapp.database.dao.DepartmentDao
import com.volkov.diplomaapp.database.entity.DepartmentEntity
import io.reactivex.Single
import javax.inject.Inject

class DepartmentsRepository @Inject constructor(
    private val departmentDao: DepartmentDao
) : Repository<DepartmentEntity> {
    override fun add(list: List<DepartmentEntity>) {
        departmentDao.add(list)
    }

    override fun update(vararg t: DepartmentEntity) {
        departmentDao.update(*t)
    }

    override fun remove(vararg t: DepartmentEntity) {
        departmentDao.remove(*t)
    }

    fun getDepartmentsByRegion(regionId: String): Single<List<DepartmentEntity>> {
        return departmentDao.getDepartmensByRegion(regionId)
    }

}