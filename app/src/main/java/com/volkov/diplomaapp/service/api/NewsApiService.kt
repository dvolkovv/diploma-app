package com.volkov.diplomaapp.service.api

import android.net.Uri
import com.volkov.diplomaapp.BuildConfig
import com.volkov.diplomaapp.database.entity.NewsEntity
import com.volkov.diplomaapp.service.api.responses.NewsDetailInfoResponse
import com.volkov.diplomaapp.service.api.responses.NewsItemResponse
import io.reactivex.Single
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

class NewsApiService {

    fun getNewsFromRemote(): List<NewsItemResponse> {
        val doc = Jsoup.connect(BuildConfig.BASE_NEWS_URL).get()
        return doc.getElementsByClass("sl-item").let(::parseNews)
    }

    fun getNewsDetailInfoFromRemote(entity: NewsEntity): Single<NewsDetailInfoResponse> {
        return Single.fromCallable {
            val doc = Jsoup.connect(entity.url).get()
            parseDetailInfo(doc)
        }
    }

    private fun parseDetailInfo(doc: Document): NewsDetailInfoResponse {
        val content: String
        val videoUrls: List<String>
        val photoUrls: List<Uri> = doc.select(".cboxElement")
            .select("a").map { it.attr("href") }.map { it.trim(); "https:$it" }.map(Uri::parse)
        doc.select(".cboxElement")
            .select("img").remove()
        doc.getElementsByClass("article").let {
            it.select("img").remove()
            content = it.html()
            videoUrls = listOf(it.select("iframe").attr("src"))
        }
        return NewsDetailInfoResponse(content, videoUrls, photoUrls)
    }

    private fun parseNews(elements: Elements): List<NewsItemResponse> {
        val resultList = ArrayList<NewsItemResponse>()
        elements.forEach { element: Element ->
            element.select("span").remove()
            var url: String
            var title: String
            val dateString: String = element.getElementsByClass("sl-item-date").text()
            element.getElementsByClass("sl-item-title").let {
                url = it.select("a").first().attr("abs:href")
                title = it.select("a").first().text()
            }

            val newsEntity = NewsItemResponse(
                date = dateString,
                title = title,
                url = url
            )
            resultList.add(newsEntity)
        }
        return resultList
    }
}