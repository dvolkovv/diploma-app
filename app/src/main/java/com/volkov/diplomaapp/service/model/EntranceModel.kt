package com.volkov.diplomaapp.service.model

import android.content.Context
import javax.inject.Inject

const val preferencesName = "diplomaapp_shared_prefs"

private const val firstLaunchKey = "isAppFirstLaunch"

class EntranceModel @Inject constructor(
    var context: Context
) {

    private val sharedPref = context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE)

    var isFirstLaunch: Boolean = sharedPref.getBoolean(firstLaunchKey, true)
        set(value) {
            sharedPref.edit()
                .putBoolean(firstLaunchKey, value)
                .apply()
        }

}