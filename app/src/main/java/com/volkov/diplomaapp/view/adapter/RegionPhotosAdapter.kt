package com.volkov.diplomaapp.view.adapter

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Point
import android.graphics.Rect
import android.graphics.RectF
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.utils.getPhotoIdByString
import com.volkov.diplomaapp.utils.getProgressDrawable
import com.volkov.diplomaapp.utils.toggleVisibility
import kotlinx.android.synthetic.main.region_photo_view_holder.view.*
import java.util.*

class RegionPhotosAdapter(private var data: List<String> = ArrayList()) :
    RecyclerView.Adapter<RegionPhotosAdapter.PhotoViewHolder>() {

    var containerView: View? = null
    private var currentAnimator: Animator? = null
    private var shortAnimationDuration: Int = 300

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.region_photo_view_holder, parent, false)
        return PhotoViewHolder(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(data[position])
        with(holder.itemView) {
            val imageId = this.context.getPhotoIdByString(data[position])
            setOnClickListener {
                zoomImage(
                    imageId,
                    this.imageThumb,
                    containerView!!.findViewById(R.id.imageExpanded)
                )
            }
        }
    }

    fun setData(data: List<String>) {
        this.data = data
        notifyDataSetChanged()
    }

    private fun zoomImage(
        imageResource: Int,
        smallImage: ImageView,
        expandedImage: ImageView
    ) {
        currentAnimator?.cancel()
        expandedImage.setImageResource(imageResource)
        val startBoundsInt = Rect()
        val finalBoundsInt = Rect()
        val globalOffset = Point()

        smallImage.getGlobalVisibleRect(startBoundsInt)
        containerView?.getGlobalVisibleRect(finalBoundsInt, globalOffset)
        startBoundsInt.offset(-globalOffset.x, -globalOffset.y)
        finalBoundsInt.offset(-globalOffset.x, -globalOffset.y)

        val startBounds = RectF(startBoundsInt)
        val finalBounds = RectF(finalBoundsInt)
        val startScale: Float
        if ((finalBounds.width() / finalBounds.height() > startBounds.width() / startBounds.height())) {
            // Extend start bounds horizontally
            startScale = startBounds.height() / finalBounds.height()
            val startWidth: Float = startScale * finalBounds.width()
            val deltaWidth: Float = (startWidth - startBounds.width()) / 2
            startBounds.left -= deltaWidth.toInt()
            startBounds.right += deltaWidth.toInt()
        } else {
            // Extend start bounds vertically
            startScale = startBounds.width() / finalBounds.width()
            val startHeight: Float = startScale * finalBounds.height()
            val deltaHeight: Float = (startHeight - startBounds.height()) / 2f
            startBounds.top -= deltaHeight.toInt()
            startBounds.bottom += deltaHeight.toInt()
        }

        smallImage.alpha = 0f
        expandedImage.run {
            visibility = View.VISIBLE
            pivotX = 0f
            pivotY = 0f
        }

        currentAnimator = AnimatorSet().apply {
            containerView?.findViewById<View>(R.id.imageBackground)?.toggleVisibility(true)
            play(
                ObjectAnimator.ofFloat(
                    expandedImage,
                    View.X,
                    startBounds.left,
                    finalBounds.left
                )
            ).apply {
                with(
                    ObjectAnimator.ofFloat(
                        expandedImage,
                        View.Y,
                        startBounds.top,
                        finalBounds.top
                    )
                )
                with(ObjectAnimator.ofFloat(expandedImage, View.SCALE_X, startScale, 1f))
                with(ObjectAnimator.ofFloat(expandedImage, View.SCALE_Y, startScale, 1f))
            }
            duration = shortAnimationDuration.toLong()
            interpolator = DecelerateInterpolator()
            addListener(object : AnimatorListenerAdapter() {

                override fun onAnimationEnd(animation: Animator) {
                    currentAnimator = null
                }

                override fun onAnimationCancel(animation: Animator) {
                    currentAnimator = null
                }
            })
            start()
        }

        expandedImage.setOnClickListener {
            currentAnimator?.cancel()
            containerView?.findViewById<View>(R.id.imageBackground)?.toggleVisibility(false)
            // Animate the four positioning/sizing properties in parallel,
            // back to their original values.
            currentAnimator = AnimatorSet().apply {
                play(ObjectAnimator.ofFloat(expandedImage, View.X, startBounds.left)).apply {
                    with(ObjectAnimator.ofFloat(expandedImage, View.Y, startBounds.top))
                    with(ObjectAnimator.ofFloat(expandedImage, View.SCALE_X, startScale))
                    with(ObjectAnimator.ofFloat(expandedImage, View.SCALE_Y, startScale))
                }
                duration = shortAnimationDuration.toLong()
                interpolator = DecelerateInterpolator()
                addListener(object : AnimatorListenerAdapter() {

                    override fun onAnimationEnd(animation: Animator) {
                        smallImage.alpha = 1f
                        expandedImage.visibility = View.GONE
                        currentAnimator = null
                    }

                    override fun onAnimationCancel(animation: Animator) {
                        smallImage.alpha = 1f
                        expandedImage.visibility = View.GONE
                        currentAnimator = null
                    }
                })
                start()
            }
        }

    }


    class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(imagePath: String) = with(itemView) {
            context.loadWithGlide(itemView.imageThumb, imagePath)
        }
    }
}

private fun Context.loadWithGlide(into: ImageView, imagePath: String) {
    val imageId = this.getPhotoIdByString(imagePath)
    Glide.with(this).load(imageId).placeholder(getProgressDrawable(this))
        .transform(CenterCrop(), RoundedCorners(10)).into(into)
}