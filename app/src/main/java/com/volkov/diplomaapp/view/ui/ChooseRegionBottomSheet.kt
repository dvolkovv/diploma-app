package com.volkov.diplomaapp.view.ui


import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.RegionEntity
import com.volkov.diplomaapp.view.adapter.RegionsAdapter
import com.volkov.diplomaapp.viewmodel.ChooseRegionViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.choose_region_bottom_sheet.*
import java.util.*


class ChooseRegionBottomSheet : Fragment() {

    var bottomSheetBehavior: BottomSheetBehavior<View>? = null
        set(value) {
            onBottomSheetBehaviorSet()
            field = value
        }

    var regionSelectedListener: ((region: RegionEntity) -> Unit)? = null
    private lateinit var viewModel: ChooseRegionViewModel
    private val subscriptions = CompositeDisposable()
    private val recyclerAdapter = RegionsAdapter()
    private val bottomSheetCallbacks: LinkedList<(state: Int) -> Unit> = LinkedList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(ChooseRegionViewModel::class.java)
        return inflater.inflate(R.layout.choose_region_bottom_sheet, container, false)
    }

    override fun onStart() {
        super.onStart()
        subscriptions.addAll(subscribeToQueryText(), subscribeToRegions())
        closeButton.setOnClickListener {
            dismiss()
        }
        initRecycler()
        initSearch()
    }

    override fun onStop() {
        super.onStop()
        closeKeyboard()
        subscriptions.clear()
    }

    fun addOnStateChangedCallback(func: (state: Int) -> Unit) {
        bottomSheetCallbacks.add(func)
        updateCallback()
    }

    fun show() {
        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    fun dismiss() {
        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun onBottomSheetBehaviorSet() {
        addOnStateChangedCallback { state: Int ->
            if (state == BottomSheetBehavior.STATE_COLLAPSED) {
                closeKeyboard()
            }
        }
    }

    private fun updateCallback() {
        bottomSheetBehavior?.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {
            }

            override fun onStateChanged(p0: View, p1: Int) {
                bottomSheetCallbacks.forEach { it.invoke(p1) }
            }
        })
    }

    private fun initRecycler() {
        regionsRecycler.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        regionsRecycler.adapter = recyclerAdapter
        with(recyclerAdapter) {
            onRegionSelectedListener = {
                regionSelectedListener?.invoke(it)
            }
            onFilteredListener = { count ->
                emptyDataIndicator.visibility =
                    if (count == 0) View.VISIBLE else View.GONE
            }
        }
    }

    private fun initSearch() {
        searchView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.updateQueryText(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun closeKeyboard() {
        val imm: InputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    private fun subscribeToRegions() =
        viewModel.regions.subscribe(recyclerAdapter::setItems)

    private fun subscribeToQueryText() =
        viewModel.getQueryText().subscribe(recyclerAdapter.filter::filter)

}
