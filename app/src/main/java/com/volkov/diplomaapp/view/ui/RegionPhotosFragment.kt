package com.volkov.diplomaapp.view.ui


import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.utils.toggleVisibility
import com.volkov.diplomaapp.view.adapter.RegionPhotosAdapter
import com.volkov.diplomaapp.view.interfaces.TitledFragment
import com.volkov.diplomaapp.viewmodel.RegionDetailViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_region_photo.*

private const val FRAGMENT_TITLE_KEY = "region_title_key"

class RegionPhotosFragment : Fragment(), TitledFragment {

    private lateinit var viewModel: RegionDetailViewModel
    private val photoAdapter = RegionPhotosAdapter()
    private val subscriptions = CompositeDisposable()

    companion object {
        fun newInstance(fragmentTitle: String): RegionPhotosFragment {
            val fragment = RegionPhotosFragment()
            fragment.arguments = Bundle().apply { putString(FRAGMENT_TITLE_KEY, fragmentTitle) }
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(parentFragment!!).get(RegionDetailViewModel::class.java)
        return inflater.inflate(R.layout.fragment_region_photo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(regionPhotoRecycler) {
            layoutManager = GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false)
            adapter = photoAdapter.also { it.containerView = view }
        }
    }

    override fun onStart() {
        super.onStart()
        subscriptions.addAll(subscribeToRegionPhotos())
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()
    }

    override fun getTitle(): String = arguments?.getString(FRAGMENT_TITLE_KEY) ?: ""

    private fun onRegionPhotosLoaded(photos: List<String>?) {
        emptyRegionPhotos.toggleVisibility(photos.isNullOrEmpty())
        photoAdapter.setData(photos.orEmpty())
    }

    private fun subscribeToRegionPhotos() =
        viewModel.getCurrentRegionPhotos().subscribe(::onRegionPhotosLoaded)

}
