package com.volkov.diplomaapp.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.RegionEntity
import kotlinx.android.synthetic.main.region_view_holder.view.*

class RegionsAdapter : RecyclerView.Adapter<RegionsAdapter.RegionViewHolder>(),
    Filterable {

    private var data = ArrayList<RegionEntity>()
    private var filteredData = data
    var onFilteredListener: ((count: Int) -> Unit)? = null
    var onRegionSelectedListener: ((RegionEntity) -> Unit)? = null

    fun setItems(items: List<RegionEntity>?) {
        data.clear()
        if (items != null) {
            data.addAll(items)
        }
        notifyDataSetChanged()
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(query: CharSequence?): FilterResults {
                val filteredResults: ArrayList<RegionEntity>
                filteredResults = if (query.isNullOrEmpty()) {
                    data
                } else {
                    data.filter {
                        return@filter it.title.contains(query.trim(), true)
                    } as ArrayList<RegionEntity>
                }
                val filterResult = FilterResults()
                filterResult.values = filteredResults
                return filterResult
            }

            override fun publishResults(p0: CharSequence?, filterResults: FilterResults?) {
                filteredData = if (filterResults == null) {
                    arrayListOf()
                } else {
                    filterResults.values as? ArrayList<RegionEntity> ?: arrayListOf()
                }
                onFilteredListener?.invoke(filteredData.size)

                notifyDataSetChanged()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.region_view_holder,
            parent,
            false
        )
        val viewHolder = RegionViewHolder(view)
        view.setOnClickListener {
            filteredData[viewHolder.adapterPosition].let {
                onRegionSelectedListener?.invoke(it)
            }
        }
        return viewHolder
    }

    override fun getItemCount(): Int = filteredData.size

    override fun onBindViewHolder(holder: RegionViewHolder, position: Int) =
        holder.bind(filteredData[position])

    class RegionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(entity: RegionEntity) {
            itemView.regionTitle.text = entity.title
            itemView.regionArea.text =
                itemView.resources.getString(R.string.region_area, entity.area)
        }
    }

}

