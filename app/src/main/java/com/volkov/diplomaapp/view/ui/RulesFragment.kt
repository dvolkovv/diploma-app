package com.volkov.diplomaapp.view.ui


import android.arch.lifecycle.ViewModelProviders
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.RuleEntity
import com.volkov.diplomaapp.utils.*
import com.volkov.diplomaapp.viewmodel.RulesViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_rules.*
import kotlinx.android.synthetic.main.rule_view_holder.view.*

class RulesFragment : Fragment() {
    private lateinit var viewModel: RulesViewModel
    private val subscriptions = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(RulesViewModel::class.java)
        return inflater.inflate(R.layout.fragment_rules, container, false)
    }

    override fun onStart() {
        super.onStart()
        subscriptions.addAll(
            subscribeToRules(),
            subscribeToLoading(),
            subscribeToException()
        )
    }

    override fun onStop() {
        super.onStop()
        subscriptions.dispose()
    }

    private fun onDataReceived(data: List<RuleEntity>) {
        rulesContainer.removeAllViewsInLayout()
        data.map { rule ->
            createRuleView(rule)
        }.forEach {
            rulesContainer.addView(it)
        }
    }

    private fun createRuleView(rule: RuleEntity): ViewGroup {
        val viewGroup = layoutInflater.inflate(
            R.layout.rule_view_holder,
            this.rulesContainer, false
        ) as ViewGroup
        return viewGroup.apply {
            bindRuleView(this, rule)
        }
    }

    private fun bindRuleView(view: ViewGroup, rule: RuleEntity) {
        view.apply {
            setOnClickListener {
                view.content.flipVisibility()
            }
            title.text = rule.title
            contentText.text = rule.content.fromHtml()
            contentImages.addImages(rule.images)
        }
    }

    private fun LinearLayout.addImages(imagePaths: List<Uri>) {
        imagePaths.forEach { imagePath ->
            val imageView = ImageView(context).apply {
                id = View.generateViewId()
                layoutParams = LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT
                )
                adjustViewBounds = true
                Glide.with(this)
                    .load(imagePath)
                    .centerCrop().fitCenter()
                    .placeholder(getProgressDrawable(context))
                    .into(this)
            }
            this.addView(imageView)
        }
    }

    private fun subscribeToRules() = viewModel.rulesObservable.subscribe(this::onDataReceived)

    private fun subscribeToLoading() =
        viewModel.getLoadingObservable().subscribe(loadingView::toggleVisibility)

    private fun subscribeToException() =
        viewModel.getExceptionObservable().subscribe(ExceptionHandler(context)::accept)

}
