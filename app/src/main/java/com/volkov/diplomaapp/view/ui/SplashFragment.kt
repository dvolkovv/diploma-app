package com.volkov.diplomaapp.view.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.viewmodel.EntranceViewModel
import com.volkov.diplomaapp.viewmodel.SplashViewModel

class SplashFragment : Fragment() {

    private lateinit var entranceViewModel: EntranceViewModel
    private lateinit var splashViewModel: SplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        ViewModelProviders.of(this).let {
            entranceViewModel = it.get(EntranceViewModel::class.java)
            splashViewModel = it.get(SplashViewModel::class.java)
        }
        return inflater.inflate(R.layout.splash_screen_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val isFirstLaunch = splashViewModel.isAppFirstLaunch()
        val navController: NavController = NavHostFragment.findNavController(this)

        val opts = NavOptions.Builder()
            .setPopUpTo(R.id.splashFragment, true)
            .build()
        Handler().postDelayed({
            if (!isFirstLaunch) {
                navController.navigate(R.id.action_to_main_app, null, opts)
            } else {
                navController.navigate(R.id.action_splashFragment_to_entranceFragment, null, opts)
            }
        }, 500)

    }
}

