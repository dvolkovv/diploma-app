package com.volkov.diplomaapp.view.ui


import android.arch.lifecycle.ViewModelProviders
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.NewsEntity
import com.volkov.diplomaapp.utils.ExceptionHandler
import com.volkov.diplomaapp.utils.fromHtml
import com.volkov.diplomaapp.utils.getProgressDrawable
import com.volkov.diplomaapp.utils.toggleVisibility
import com.volkov.diplomaapp.viewmodel.NewsDetailViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_news_detail.*


private const val NEWS_ID_PARAM = "news_id"

class NewsDetailFragment : Fragment() {

    private lateinit var viewModel: NewsDetailViewModel
    private val subscriptions = CompositeDisposable()

    companion object {
        fun createBundle(newsId: Int) = Bundle().apply { putInt(NEWS_ID_PARAM, newsId) }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(NewsDetailViewModel::class.java)
        return inflater.inflate(R.layout.fragment_news_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments!!.getInt(NEWS_ID_PARAM).let(viewModel::createNewsObservable)
    }

    override fun onStart() {
        super.onStart()
        subscriptions.addAll(subscribeToNews(), subscribeToLoading())
    }

    private fun onDataReceived(news: NewsEntity) {
        news.info?.let {
            newsContent?.text = it.content.fromHtml()
            it.photoUrl?.forEach { path ->
                val imageView = createImageView(path)
                addImageView(imageView)
            }
        }
    }

    private fun addImageView(imageView: ImageView?) {
        if (imageView == null) {
            return
        }
        newsDetailsContainer?.addView(imageView)
    }

    private fun createImageView(imagePath: Uri): ImageView? {
        if (context == null) {
            return null
        }
        return ImageView(context).apply {
            layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            ).also { it.setMargins(8, 8, 8, 8) }
            adjustViewBounds = true
            Glide.with(this)
                .load(imagePath)
                .fitCenter()
                .placeholder(getProgressDrawable(context))
                .into(this)
        }
    }

    private fun subscribeToNews() =
        viewModel.newsObservable?.doOnError(ExceptionHandler(context)::accept)?.subscribe(::onDataReceived)

    private fun subscribeToLoading() =
        viewModel.getLoadingObservable().subscribe(loading::toggleVisibility)

}
