package com.volkov.diplomaapp.view.ui


import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.RegionEntity
import com.volkov.diplomaapp.utils.toggleVisibility
import com.volkov.diplomaapp.view.interfaces.TitledFragment
import com.volkov.diplomaapp.viewmodel.RegionDetailViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_region_detail_program.*

private const val FRAGMENT_TITLE_KEY = "description_title_key"

class RegionDetailProgramFragment : Fragment(), TitledFragment {
    private lateinit var viewModel: RegionDetailViewModel
    private val subscriptions = CompositeDisposable()

    companion object {
        fun newInstance(fragmentTitle: String): RegionDetailProgramFragment {
            val fragment = RegionDetailProgramFragment()
            fragment.arguments = Bundle().apply { putString(FRAGMENT_TITLE_KEY, fragmentTitle) }
            return fragment
        }
    }

    override fun getTitle(): String = arguments?.getString(FRAGMENT_TITLE_KEY) ?: ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(parentFragment!!).get(RegionDetailViewModel::class.java)
        return inflater.inflate(R.layout.fragment_region_detail_program, container, false)
    }

    override fun onStart() {
        super.onStart()
        subscriptions.addAll(subscribeToCurrentRegion())
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()
    }

    private fun initViews(region: RegionEntity) {
        brief_program.text = region.briefProgram
        findDetailPrograms.toggleVisibility(region.regionDetailSite != null)
        findDetailPrograms.setAnimatedClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(region.regionDetailSite)
                )
            )
        }
    }

    private fun onRegionLoaded(region: RegionEntity) {
        val regionEmpty = region.isDescriptionInfoEmpty()
        if (regionEmpty) {
            showEmpty()
        } else {
            initViews(region)
        }
    }

    private fun showEmpty() {
        emptyDescription.toggleVisibility(true)
    }

    private fun subscribeToCurrentRegion() =
        viewModel.getCurrentRegionEntity().subscribe(::onRegionLoaded)


}
