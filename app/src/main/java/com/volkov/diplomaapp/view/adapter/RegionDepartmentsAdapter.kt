package com.volkov.diplomaapp.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.DepartmentEntity
import kotlinx.android.synthetic.main.department_view_holder.view.*
import kotlinx.android.synthetic.main.sub_region_view_holder.view.*


private const val ITEM_SUBREGION = 1
private const val ITEM_DEPARTMENT = 0

class RegionDepartmentsAdapter(
    data: List<DepartmentEntity> = ArrayList(),
    private val onDepartmentClicked: ((String) -> Unit)? = null
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dataWithHolders = ArrayList<RecyclerItem>()

    init {
        mapDataToHolders(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            ITEM_SUBREGION -> {
                inflater.inflate(
                    R.layout.sub_region_view_holder,
                    parent,
                    false
                ).let { return SubRegionViewHolder(it) }
            }
            ITEM_DEPARTMENT -> {
                inflater.inflate(
                    R.layout.department_view_holder,
                    parent,
                    false
                ).let { return DepartmentViewHolder(it) }
            }
            else -> throw IllegalStateException("Wrong viewType!")
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (dataWithHolders[position].subRegionTitle != null) {
            return ITEM_SUBREGION
        }
        return ITEM_DEPARTMENT
    }

    override fun getItemCount() = dataWithHolders.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = dataWithHolders[position]
        when (holder) {
            is SubRegionViewHolder -> {
                holder.bind(data.subRegionTitle!!)
            }
            is DepartmentViewHolder -> {
                holder.bind(data.departmentEntity!!)
            }
        }
    }

    fun setData(data: List<DepartmentEntity>) {
        dataWithHolders.clear()
        mapDataToHolders(data)
        notifyDataSetChanged()
    }

    private fun mapDataToHolders(data: List<DepartmentEntity>) {
        data.forEach {
            val recyclerItem = RecyclerItem(it.subRegion)
            with(dataWithHolders) {
                if (!this.contains(recyclerItem)) {
                    add(recyclerItem)
                    add(RecyclerItem(departmentEntity = it))
                } else {
                    add(RecyclerItem(departmentEntity = it))
                }
            }
        }
    }

    private data class RecyclerItem(
        val subRegionTitle: String? = null,
        val departmentEntity: DepartmentEntity? = null
    )

    class SubRegionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(title: String) {
            itemView.departmentRegion.text = title
        }
    }

    inner class DepartmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: DepartmentEntity) = with(itemView) {
            setOnClickListener {
                onDepartmentClicked?.invoke(item.oid)
            }
            address.text = item.address
            phoneNumber.text = item.phoneNumber
        }
    }
}