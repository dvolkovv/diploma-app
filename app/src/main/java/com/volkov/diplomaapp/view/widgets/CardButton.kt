package com.volkov.diplomaapp.view.widgets

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.TextView
import com.volkov.diplomaapp.R

class CardButton(context: Context, attrs: AttributeSet) : AnimatedCardView(context, attrs) {
    init {
        inflate(context, R.layout.card_button, this)
        useCompatPadding = true
        cardElevation = resources.getDimension(R.dimen.button_elevation)

        val imageView: ImageView = findViewById(R.id.image)
        val textView: TextView = findViewById(R.id.text)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.CardButton)
        imageView.setImageDrawable(attributes.getDrawable(R.styleable.CardButton_image))
        textView.text = attributes.getString(R.styleable.CardButton_text)
        attributes.recycle()
    }

}