package com.volkov.diplomaapp.view.widgets

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import com.volkov.diplomaapp.R


open class AnimatedCardView(context: Context, attrs: AttributeSet?) : CardView(context, attrs) {

    init {
        useCompatPadding = true
    }

    constructor(context: Context) : this(context, null)

    fun setAnimatedClickListener(func: () -> Unit) {
        val set =
            AnimatorInflater.loadAnimator(context, R.animator.button_press_animator) as AnimatorSet
        set.setTarget(this)
        this.setOnClickListener {
            set.start()
            func.invoke()
        }
    }
}