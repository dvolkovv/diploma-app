package com.volkov.diplomaapp.view.ui


import android.arch.lifecycle.ViewModelProviders
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.view.adapter.RegionDetailPagerAdapter
import com.volkov.diplomaapp.viewmodel.RegionDetailViewModel
import kotlinx.android.synthetic.main.fragment_region_detail.*

const val REGION_ID_PARAM = "region_id"

class RegionDetailFragment : Fragment() {

    companion object {
        fun createBundle(regionId: String) = Bundle().apply { putString(REGION_ID_PARAM, regionId) }
    }

    private lateinit var viewModel: RegionDetailViewModel
    private var actionBar: ActionBar? = null
    private var regionId: String? = null
    private var pagerAdapter: RegionDetailPagerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        regionId = arguments?.getString(REGION_ID_PARAM)
        viewModel = ViewModelProviders.of(parentFragment!!).get(RegionDetailViewModel::class.java)
        viewModel.selectedRegionId = regionId
        pagerAdapter = fragmentManager?.let {
            RegionDetailPagerAdapter(
                it,
                listOf(
                    RegionDescriptionFragment.newInstance(getString(R.string.description)),
                    RegionDetailProgramFragment.newInstance(getString(R.string.programs)),
                    RegionDepartmentsFragment.newInstance(getString(R.string.departments)),
                    RegionPhotosFragment.newInstance(getString(R.string.photo))
                )
            )
        }
        return inflater.inflate(R.layout.fragment_region_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initActionBar()
        initViews()
    }

    private fun initActionBar() {
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            toolbar.navigationIcon?.setColorFilter(
                ResourcesCompat.getColor(resources, android.R.color.white, null),
                PorterDuff.Mode.SRC_ATOP
            )
            it.title = regionId
        }
    }

    private fun initViews() {
        regionDetailViewPager.adapter = pagerAdapter
    }

}
