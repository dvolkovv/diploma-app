package com.volkov.diplomaapp.view.interfaces

interface TitledFragment {

    fun getTitle(): String
}
