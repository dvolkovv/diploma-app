package com.volkov.diplomaapp.view.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.utils.toggleVisibility
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    var onBackPressedFragmentAction: (() -> Boolean)? = null
    var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        navController = container.findNavController()
        navController?.let {
            it.addOnDestinationChangedListener { _, destination, _ ->
                handleDestination(destination)
            }
            bottomNavigationBar.setupWithNavController(it)
        }
        bottomNavigationBar.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.rules_nav_graph -> {
                    navController?.setGraph(R.navigation.rules_nav_graph)
                    true
                }
                R.id.regions_nav_graph -> {
                    navController?.setGraph(R.navigation.regions_nav_graph)
                    true
                }
                R.id.news_nav_graph -> {
                    navController?.setGraph(R.navigation.news_nav_graph)
                    true
                }
                else -> false
            }

        }
    }

    override fun onBackPressed() {
        if (onBackPressedFragmentAction == null) {
            super.onBackPressed()
        } else {
            onBackPressedFragmentAction?.let {
                if (!it.invoke()) {
                    super.onBackPressed()
                }
            }
        }
    }

    override fun onSupportNavigateUp() = findNavController(R.id.container).navigateUp()

    private fun handleDestination(destination: NavDestination) {
        when (destination.parent?.id) {
            R.id.entranceNavigation,
            R.id.regionDetailsNavigation -> bottomNavigationBar.toggleVisibility(false)
            else -> bottomNavigationBar.toggleVisibility(true)
        }
    }

}
