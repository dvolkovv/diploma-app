package com.volkov.diplomaapp.view.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.viewmodel.EntranceViewModel
import kotlinx.android.synthetic.main.fragment_entrance.*

class EntranceFragment : Fragment() {

    private lateinit var viewModel: EntranceViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(EntranceViewModel::class.java)
        return inflater.inflate(R.layout.fragment_entrance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        continue_button.setOnClickListener {
            viewModel.setAppFirstLaunch(false)
            findNavController().navigate(R.id.action_to_main_app_with_pop)
        }
    }
}