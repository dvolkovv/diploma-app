package com.volkov.diplomaapp.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.volkov.diplomaapp.view.interfaces.TitledFragment

class RegionDetailPagerAdapter(
    fm: FragmentManager,
    private val fragmentList: List<Fragment>
) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int = fragmentList.size

    override fun getPageTitle(position: Int): CharSequence? =
        (fragmentList[position] as TitledFragment).getTitle()

}