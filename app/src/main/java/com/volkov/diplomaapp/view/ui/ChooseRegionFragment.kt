package com.volkov.diplomaapp.view.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.volkov.diplomaapp.DiplomaApp
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.utils.MapsHelper
import kotlinx.android.synthetic.main.fragment_choose_region.*
import javax.inject.Inject

class ChooseRegionFragment : Fragment() {

    @Inject
    lateinit var mapsHelper: MapsHelper
    private var bottomSheet: ChooseRegionBottomSheet? = null

    init {
        DiplomaApp.appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_choose_region, container, false)
    }

    override fun onStart() {
        super.onStart()
        initViews()
        setupBackAction()
    }

    override fun onStop() {
        super.onStop()
        removeBackAction()
    }

    private fun initViews() {
        mapsHelper.let {
            it.initMaps(webView)
            it.listener = this::onRegionClicked
        }
        initBottomSheet()
        tap_outside.setOnClickListener {
            bottomSheet?.dismiss()
            tap_outside.toggleVisibilityWithAnim(false)
        }
        searchButton.setOnClickListener {
            bottomSheet?.show()
        }
    }

    private fun initBottomSheet() {
        bottomSheet =
            childFragmentManager.findFragmentById(R.id.sheet_fragment) as ChooseRegionBottomSheet?
        bottomSheet?.bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheet?.regionSelectedListener = {
            bottomSheet?.dismiss()
            navigateToRegion(it.title)
        }
        bottomSheet?.addOnStateChangedCallback { state ->
            when (state) {
                BottomSheetBehavior.STATE_EXPANDED -> {
                    regionSelected.hide()
                    tap_outside.toggleVisibilityWithAnim(true)
                }
                BottomSheetBehavior.STATE_COLLAPSED -> {
                    if (regionSelected != null && regionSelected.isTitleSet()) {
                        regionSelected.show()
                    }
                    tap_outside.toggleVisibilityWithAnim(false)
                }
            }
        }
    }

    private fun onRegionClicked(regionTitle: String) {
        if (regionSelected.isVisible()) {
            navigateToRegion(regionTitle)
        } else {
            regionSelected.showWithTitle(regionTitle)
        }
    }

    private fun navigateToRegion(regionTitle: String) {
        val mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post {
            findNavController().navigate(
                R.id.regionDetailsNavigation,
                RegionDetailFragment.createBundle(regionTitle)
            )
        }
    }

    private fun setupBackAction() {
        val mainActivity = activity as? MainActivity
        mainActivity?.onBackPressedFragmentAction = {
            bottomSheet?.dismiss()
            true
        }
    }

    private fun removeBackAction() {
        val mainActivity = activity as? MainActivity
        mainActivity?.onBackPressedFragmentAction = null
    }

    private fun View?.toggleVisibilityWithAnim(isVisible: Boolean) {
        if (this == null) {
            return
        }
        if (isVisible) {
            this.animate().withStartAction {
                alpha = 0f
                visibility = View.VISIBLE
                isClickable = true
            }.alpha(1f).start()
        } else {
            this.animate().withEndAction {
                visibility = View.INVISIBLE
                isClickable = false
            }.alpha(0f).start()
        }
    }
}
