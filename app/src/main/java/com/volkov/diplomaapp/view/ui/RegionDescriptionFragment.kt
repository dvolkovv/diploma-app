package com.volkov.diplomaapp.view.ui


import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.volkov.diplomaapp.BuildConfig
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.RegionEntity
import com.volkov.diplomaapp.utils.getPhotoIdByString
import com.volkov.diplomaapp.utils.getProgressDrawable
import com.volkov.diplomaapp.utils.toggleVisibility
import com.volkov.diplomaapp.view.interfaces.TitledFragment
import com.volkov.diplomaapp.viewmodel.RegionDetailViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_region_description.*
import kotlinx.android.synthetic.main.fragment_region_description.view.*

private const val FRAGMENT_TITLE_KEY = "description_title_key"

class RegionDescriptionFragment : Fragment(), TitledFragment {
    private lateinit var viewModel: RegionDetailViewModel
    private val subscriptions = CompositeDisposable()

    companion object {
        fun newInstance(fragmentTitle: String): RegionDescriptionFragment {
            val fragment = RegionDescriptionFragment()
            fragment.arguments = Bundle().apply { putString(FRAGMENT_TITLE_KEY, fragmentTitle) }
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(parentFragment!!).get(RegionDetailViewModel::class.java)
        return inflater.inflate(R.layout.fragment_region_description, container, false)
    }

    override fun onStart() {
        super.onStart()
        subscriptions.addAll(subscribeToCurrentRegion())
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()
    }

    override fun getTitle(): String = arguments?.getString(FRAGMENT_TITLE_KEY) ?: ""

    private fun initViews(region: RegionEntity) {
        findHomeButton.setAnimatedClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(BuildConfig.BASE_AVITO_URL)
                )
            )
        }
        findWorkButton.setAnimatedClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(BuildConfig.BASE_HH_URL)
                )
            )
        }
        findMedButton.setAnimatedClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(region.medAssistanceSite)
                )
            )
        }
    }

    private fun onRegionLoaded(region: RegionEntity) {
        if (region.isDescriptionInfoEmpty()) {
            emptyDescription.toggleVisibility(true)
            buttons.toggleVisibility(false)
            return
        } else {
            emptyDescription.toggleVisibility(false)
            buttons.toggleVisibility(true)
        }
        initViews(region)
        loadImage(region.photos?.firstOrNull())
        view?.run {
            descriptionText.text = region.info
        }
    }

    private fun loadImage(imagePath: String?) {
        if (imagePath == null) {
            descriptionImage.toggleVisibility(false)
            return
        }
        context?.let {
            val photoId = it.getPhotoIdByString(imagePath)
            Glide.with(this).load(photoId)
                .placeholder(getProgressDrawable(it))
                .into(this.descriptionImage)
        }
    }

    private fun subscribeToCurrentRegion() =
        viewModel.getCurrentRegionEntity().subscribe(::onRegionLoaded)

}
