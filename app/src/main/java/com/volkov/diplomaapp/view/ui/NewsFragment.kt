package com.volkov.diplomaapp.view.ui


import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.NewsEntity
import com.volkov.diplomaapp.utils.ExceptionHandler
import com.volkov.diplomaapp.view.adapter.NewsAdapter
import com.volkov.diplomaapp.viewmodel.NewsViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment : Fragment() {

    private lateinit var viewModel: NewsViewModel
    private val recyclerAdapter = NewsAdapter()
    private var subscriptions = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(NewsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        initSwipeRefresh()
    }

    override fun onStart() {
        super.onStart()
        subscriptions.addAll(
            subscribeToNews(),
            subscribeToLoading(),
            subscribeToException()
        )
    }

    override fun onStop() {
        super.onStop()
        subscriptions.dispose()
    }

    private fun subscribeToLoading() =
        viewModel.getLoadingObservable().subscribe { isLoading ->
            if (isLoading) {
                newsLoadingProgress?.visibility = View.VISIBLE
                newsRecycler?.visibility = View.GONE
            } else {
                newsLoadingProgress?.visibility = View.GONE
                newsRecycler?.visibility = View.VISIBLE
            }
        }

    private fun subscribeToException() =
        viewModel.getExceptionObservable().subscribe(ExceptionHandler(context)::accept)

    private fun subscribeToNews() =
        viewModel.newsObservable
            .subscribe(::onDataLoaded)

    private fun subscribeToRefreshing() =
        viewModel.refreshNews().doOnEvent { swipeRefresh?.isRefreshing = false }
            .subscribe()

    private fun initSwipeRefresh() {
        swipeRefresh.setOnRefreshListener {
            subscribeToRefreshing()
        }
    }

    private fun onDataLoaded(data: List<NewsEntity>) {
        recyclerAdapter.submitList(data)
    }

    private fun initRecycler() {
        val clickListener: ((Int) -> Unit) = { newsId ->
            findNavController().navigate(
                R.id.newsDetailFragment,
                NewsDetailFragment.createBundle(newsId)
            )
        }
        with(newsRecycler) {
            adapter = recyclerAdapter.also { it.clickListener = clickListener }
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }
}
