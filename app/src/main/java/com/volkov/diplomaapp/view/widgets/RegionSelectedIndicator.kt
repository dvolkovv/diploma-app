package com.volkov.diplomaapp.view.widgets

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.utils.setAnimationCallback

class RegionSelectedIndicator(context: Context, attrs: AttributeSet) : CardView(context, attrs) {
    init {
        inflate(context, R.layout.region_selected_indicator, this)
    }

    private val titleTextView: TextView = findViewById(R.id.title)

    fun isTitleSet(): Boolean = titleTextView.text.isNullOrEmpty().not()

    fun isVisible(): Boolean = this.visibility == View.VISIBLE

    fun show() {
        if (visibility == View.VISIBLE) {
            return
        }
        val slideIn = AnimationUtils.loadAnimation(context, R.anim.slide_in)
        slideIn.setAnimationCallback(onStart = { visibility = View.VISIBLE })
        startAnimation(slideIn)
    }

    fun showWithTitle(regionTitle: String?) {
        val oldText = titleTextView.text
        if (oldText != regionTitle) {
            titleTextView.text = regionTitle
        }
        show()
    }

    fun hide() {
        if (visibility == View.GONE) {
            return
        }
        val slideOut = AnimationUtils.loadAnimation(context, R.anim.slide_out)
        slideOut.setAnimationCallback(onEnd = { visibility = View.GONE })
        startAnimation(slideOut)
    }
}