package com.volkov.diplomaapp.view.ui


import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.DepartmentEntity
import com.volkov.diplomaapp.utils.ExceptionHandler
import com.volkov.diplomaapp.utils.toggleVisibility
import com.volkov.diplomaapp.view.adapter.RegionDepartmentsAdapter
import com.volkov.diplomaapp.view.interfaces.TitledFragment
import com.volkov.diplomaapp.viewmodel.RegionDetailViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_region_departments.*


private const val FRAGMENT_TITLE_KEY = "region_title_key"

class RegionDepartmentsFragment : Fragment(), TitledFragment {
    private lateinit var viewModel: RegionDetailViewModel
    private val departmentsAdapter =
        RegionDepartmentsAdapter(onDepartmentClicked = ::openYandexMaps)
    private val subscriptions = CompositeDisposable()

    companion object {
        fun newInstance(fragmentTitle: String): RegionDepartmentsFragment {
            val fragment = RegionDepartmentsFragment()
            fragment.arguments = Bundle().apply { putString(FRAGMENT_TITLE_KEY, fragmentTitle) }
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(parentFragment!!).get(RegionDetailViewModel::class.java)
        return inflater.inflate(R.layout.fragment_region_departments, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(departmentsRecycler) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = departmentsAdapter
        }
    }

    override fun onStart() {
        super.onStart()
        subscriptions.addAll(subscribeToDepartments())
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()
    }

    override fun getTitle(): String = arguments?.getString(FRAGMENT_TITLE_KEY) ?: ""

    private fun openYandexMaps(oid: String) {
        val uri = Uri.parse("yandexmaps://maps.yandex.ru/?oid=$oid")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        val packageManager = activity?.packageManager
        if (packageManager != null && intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        } else {
            Toast.makeText(requireContext(), R.string.ymaps_should_be_installed, Toast.LENGTH_LONG).show()
        }
    }

    private fun onDepartmentsLoaded(departments: List<DepartmentEntity>) {
        emptyDepartmentsIndicator.toggleVisibility(departments.isEmpty())
        departmentsAdapter.setData(departments)
    }

    private fun subscribeToDepartments() =
        viewModel.getCurrentRegionDepartments()
            .subscribe(this::onDepartmentsLoaded, ExceptionHandler(context)::accept)

}
