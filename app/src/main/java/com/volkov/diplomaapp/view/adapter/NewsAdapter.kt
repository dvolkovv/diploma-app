package com.volkov.diplomaapp.view.adapter

import android.os.Handler
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.volkov.diplomaapp.R
import com.volkov.diplomaapp.database.entity.NewsEntity
import com.volkov.diplomaapp.view.widgets.AnimatedCardView
import kotlinx.android.synthetic.main.news_view_holder.view.*

class NewsAdapter : ListAdapter<NewsEntity, NewsAdapter.NewsViewHolder>(diffUtilCallback) {

    var clickListener: ((newsId: Int) -> Unit)? = null

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): NewsViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(
            R.layout.news_view_holder,
            p0,
            false
        ) as AnimatedCardView
        val viewHolder = NewsViewHolder(view)
        view.setAnimatedClickListener {
            Handler().postDelayed(
                { clickListener?.invoke(getItem(viewHolder.adapterPosition).id) }
                , 300
            )
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: NewsEntity) {
            with(itemView) {
                news_title.text = item.title
                news_date.text = item.getDateString()
            }
        }
    }
}

private val diffUtilCallback = object : DiffUtil.ItemCallback<NewsEntity>() {
    override fun areItemsTheSame(p0: NewsEntity, p1: NewsEntity): Boolean =
        p0.id == p1.id

    override fun areContentsTheSame(p0: NewsEntity, p1: NewsEntity): Boolean =
        p0 == p1

}
